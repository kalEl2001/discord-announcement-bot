module gitlab.com/ctf-compfest-13/infrastructure/discord-announcement-bot.git

go 1.14

require (
	github.com/bwmarrin/discordgo v0.23.1 // indirect
	github.com/joho/godotenv v1.3.0 // indirect
)
