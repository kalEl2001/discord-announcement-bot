FROM golang:1.15-alpine AS builder

WORKDIR /app

COPY . .

RUN ["go", "build"]

FROM golang:1.15-alpine

COPY --from=builder /app/discord-announcement-bot app

COPY ./.env ./.env

CMD ["./app"]